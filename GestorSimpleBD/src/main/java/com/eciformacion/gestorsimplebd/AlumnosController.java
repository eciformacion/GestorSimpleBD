package com.eciformacion.gestorsimplebd;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.eciformacion.gestorsimlebd.beans.GestorAlumnos;
import com.eciformacion.gestorsimplebd.pojos.Alumno;

@Controller
@RequestMapping("/alumnos")
@SessionAttributes("loginname")
public class AlumnosController {

	@Autowired
	private GestorAlumnos gestoralumnos;
	
	@ModelAttribute("loginname")
	public String addLoginName() {
		return "victor";
	}
	
	
	@ModelAttribute("alumno")
	public Alumno addAlumno(@RequestParam("nombre") String nombre,
			@RequestParam("apellidos") String apellidos,
			@RequestParam("edad") Integer edad) {
		Alumno alumno=new Alumno();
		alumno.setNombre(nombre);
		alumno.setApellidos(apellidos);
		alumno.setEdad(edad);
		alumno.setCentro("ECI FORMACION");
		return alumno;
		
	}
	
	@PostMapping
	public String altaAlumno(
			@ModelAttribute("alumno") Alumno alumno,
			@ModelAttribute("loginname") String loginname,
			Model modelo) {
		
		loginname=alumno.getNombre();
		try {
			if(gestoralumnos.a�adirAlumno(alumno)) {
				modelo.addAttribute("resultado","Operacion a�adir realizada correctamente");
			}else{
				modelo.addAttribute("resultado","Operacion a�adir no realizada");
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			modelo.addAttribute("resultado","Operacion a�adir no realizada");
			
		};
		
		return "operacion";
	}
	@GetMapping
	public String listarAlumnos(Model modelo) {
		try {
			modelo.addAttribute("listado",gestoralumnos.obtenerAlumnos());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//modelo.addAttribute("listado",new ArrayList());
		}
		
		return "listaalumnos";
	}
}
