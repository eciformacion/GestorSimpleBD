package com.eciformacion.gestorsimlebd.beans;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eciformacion.gestorsimplebd.pojos.Alumno;

import java.sql.Statement;

@Component
public class GestorAlumnos {

	@Autowired
	private GestorDataSource datasource;

	public GestorDataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(GestorDataSource datasource) {
		this.datasource = datasource;
	}
	
	private Connection conectarBd() {
		Connection conn=null;
		try {
			conn = datasource.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	public boolean aņadirAlumno(Alumno alumno) throws SQLException {
		
		Connection conn=conectarBd();
		PreparedStatement ps=conn.prepareStatement("INSERT INTO alumnos(nombre,apellidos,edad,centro) VALUES(?,?,?,?)");
		ps.setString(1, alumno.getNombre());
		ps.setString(2, alumno.getApellidos());
		ps.setInt(3, alumno.getEdad());
		ps.setString(4, alumno.getCentro());
		int n=ps.executeUpdate();
		ps.close();
		conn.close();
		if(n>0) {
			return true;
		}else {
			return false;
		}
	}
	public ArrayList<String> obtenerAlumnos() throws SQLException{
		Connection conn=conectarBd();
		ArrayList<String> retorno=new ArrayList();
		Statement st=conn.createStatement();
		ResultSet rs=st.executeQuery("SELECT * FROM alumnos");
		while(rs.next()) {
			retorno.add(rs.getString(1));
		}
		st.close();
		conn.close();
		return retorno;
	}
	
}
